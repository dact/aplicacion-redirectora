import socket
import random

myPort = 1235
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
"""Con este comando anterior, podemos reutilizar un mismo puerto unavez cerrado el servidor """
mySocket.bind(('', myPort))

mySocket.listen(5)
lista = ['https://youtube.com/', 'https://www.aulavirtual.urjc.es', 'localhost:1234/', 'https://google.es/',
        'https://twitter.com/', 'https://twitch.com/', 'https://gsyc.urjc.es/']

try:
	while True:
		print("Waiting for connections")
		(recvSocket, address) = mySocket.accept()
		print("HTTP request received:")
		print(recvSocket.recv(2048))

		# Resource name for next url
		nextPage = random.choice(lista)
		# HTML body of the page to serve
		htmlBody = "<h1>Working!</h1>" + '<p>Next page: <a href="' \
		    + nextPage + '">' + nextPage + "</a></p>"
		recvSocket.send(b"HTTP/1.1 200 OK \r\n\r\n" +
				b"<html><body>" + htmlBody.encode('ascii') + b"</body></html>" +
				b"\r\n")
		recvSocket.close()

except KeyboardInterrupt:
	print("Closing binded socket")
	mySocket.close()